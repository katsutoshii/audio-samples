Thank you for download our free audio samples!

This promo pack includes 60 vocal samples & loops from www.lucidsamples.com. All audio samples are in wav format.

In the folder "_file-links to full sample packs" you will find links to our sample packs on www.LucidSamples.com

In the folder "_social media" you will find links to our facebook, youtube, soundcloud and Twitter - Join us for news and discounts!

(You have to double click on the files in these folders).


Samples come from:

- Cut Vocals Party (Remastered Version)
- Glitch Vocal Mash Ups
- Hardcore Voices Invasion
- K-Motion House Vocals
- Pirate MC Vocals - Melanie Jane

+ 18 Free Dj Vocals


Need more samples?

Please, visit us again at

www.lucidsamples.com