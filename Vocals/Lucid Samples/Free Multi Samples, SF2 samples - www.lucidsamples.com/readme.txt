This promo pack includes 6 free multi samples in soundfont format (SF2 format) from www.lucidsamples.com

In the folder "file-links to full sample packs" you will find links to full packs on www.LucidSamples.com

In the folder "_social media" you will find links to our facebook, youtube, soundcloud and Twitter - Join us for news and discounts!

(You have to double click on the files in these folders).


Samples come from:

- Deeper Instruments vol.1
- Electro-House Multisamples
- Fantasy Leads
- Multi Bass Waveforms
- Ultra Dance Leads vol.1
- Ultra Dance Leads vol.2


SF2: popular format, compatible with most software samplers:

- Emulator X
- FL Studio DirectWave
- HALion
- Independence
- Kontakt
- Logic Pro EXS 24
- Machfive
- Proteus X
- SFZ+
- VSampler
- and others...


SXT: Reason NN-XT [propellerhead Software]

* Cubase 'as it is' does not enable you to use SF2 format. Instead, you can use an appropriate sampler e.g. SFZ+ or NI Kontakt; you open it in Cubase as you do in the case of other plug-ins.


Need more samples?

Please, visit us again at

www.lucidsamples.com