Thank you for download our free audio samples!

This promo pack includes 89 sounds and loops from www.lucidsamples.com. All audio samples are in wav format.

In the folder "_file-links to full sample packs" you will find links to our sample packs on www.LucidSamples.com

In the folder "_social media" you will find links to our facebook, youtube, soundcloud and Twitter - Join us for news and discounts!

(You have to double click on the files in these folders).


Samples come from:

- Cut Vocals Party (HQ Edition)
- Hardcore Kicks Invasion
- Hardcore Samples Invasion
- Hardcore Voices Invasion
- Hardstyle Fx
- Hardstyle Kicks
- Hardstyle Midi (HQ Edition)


Need more samples?

Please, visit us again at

www.lucidsamples.com