//==============================================================
// Oboe Section : Normal Mod Wheel
//
//   This is a combination of
//
//     oboe-SEC-sustain.sfz
//     oboe-SEC-staccato.sfz
//
//   ... where the mod wheel lengthens the attack
//
//   by Paul Battersby - http://virtualplaying.com
//
//==============================================================

//==============================================================
//  Oboe Section : Sustain
//
//   Woodwinds - Oboes Sustain.sfz from Sonatina Symphonic Orchestra - http://sso.mattiaswestlund.net
//
//   modified by Paul Battersby - http://virtualplaying.com
//
//==============================================================

// ------------------------------
//  Sonatina Symphonic Orchestra
// ------------------------------
//        Oboes Sustain
// ------------------------------

<group>
ampeg_attackcc1=0.5 // mod wheel slows attack (PB)
volume=1
ampeg_attack=0.03
ampeg_release=0.9 // slightly longer release for sustain (PB)

// randomize like a real player(PB)
pitch_random=5
amp_random=1



// removed a#3, sounds accented (PB)

<region>
sample=..\libs\SSO\Samples\Oboes\oboes-sus-c#4-PB-loop.wav
lokey=a#3
hikey=d4
pitch_keycenter=c#4

<region>
sample=..\libs\SSO\Samples\Oboes\oboes-sus-e4-PB-loop.wav
lokey=d#4
hikey=f4
pitch_keycenter=e4

<region>
sample=..\libs\SSO\Samples\Oboes\oboes-sus-g4-PB-loop.wav
lokey=f#4
hikey=g#4
pitch_keycenter=g4

<region>
sample=..\libs\SSO\Samples\Oboes\oboes-sus-a#4-PB-loop.wav
lokey=a4
hikey=b4
pitch_keycenter=a#4

<region>
sample=..\libs\SSO\Samples\Oboes\oboes-sus-c#5-PB-loop.wav
lokey=c5
hikey=d5
pitch_keycenter=c#5

<region>
sample=..\libs\SSO\Samples\Oboes\oboes-sus-e5-PB-loop.wav
pan=-10
lokey=d#5
hikey=f5
pitch_keycenter=e5

<region>
sample=..\libs\SSO\Samples\Oboes\oboes-sus-g5-PB-loop.wav
lokey=f#5
hikey=g#5
pitch_keycenter=g5

<region>
sample=..\libs\SSO\Samples\Oboes\oboes-sus-a#5-PB-loop.wav
lokey=a5
hikey=d6
pitch_keycenter=a#5

// removed C#6, had a click sound (PB)

<region>
sample=..\libs\SSO\Samples\Oboes\oboes-sus-e6-PB-loop.wav
lokey=d#6
hikey=a6 // extend the range (PB)
pitch_keycenter=e6



//==============================================================
//  Oboe Section : Staccato
//
//   Woodwinds - Oboes Staccato.sfz from Sonatina Symphonic Orchestra - http://sso.mattiaswestlund.net
//
//   modified by Paul Battersby - http://virtualplaying.com
//
//==============================================================

// ------------------------------
//  Sonatina Symphonic Orchestra
// ------------------------------
//        Oboes Staccato
// ------------------------------

<group>
ampeg_attackcc1=0.5 // mod wheel slows attack (PB)
gain_cc1=-15        // mod wheel reduces volume (PB)
// manufacture staccato from sustain (PB)
offset=1530

ampeg_attack=0.04
ampeg_sustain=0
ampeg_hold=0.09
ampeg_decay=0.5
ampeg_release=0.5

volume=-5



// randomize like a real player(PB)
pitch_random=5
amp_random=1



// removed a#3, sounds accented (PB)

<region>
sample=..\libs\SSO\Samples\Oboes\oboes-sus-c#4-PB-loop.wav
lokey=a#3
hikey=d4
pitch_keycenter=c#4

<region>
sample=..\libs\SSO\Samples\Oboes\oboes-sus-e4-PB-loop.wav
lokey=d#4
hikey=f4
pitch_keycenter=e4

<region>
sample=..\libs\SSO\Samples\Oboes\oboes-sus-g4-PB-loop.wav
lokey=f#4
hikey=g#4
pitch_keycenter=g4

<region>
sample=..\libs\SSO\Samples\Oboes\oboes-sus-a#4-PB-loop.wav
lokey=a4
hikey=b4
pitch_keycenter=a#4

<region>
sample=..\libs\SSO\Samples\Oboes\oboes-sus-c#5-PB-loop.wav
lokey=c5
hikey=d5
pitch_keycenter=c#5

<region>
sample=..\libs\SSO\Samples\Oboes\oboes-sus-e5-PB-loop.wav
pan=-10
lokey=d#5
hikey=f5
pitch_keycenter=e5

<region>
sample=..\libs\SSO\Samples\Oboes\oboes-sus-g5-PB-loop.wav
lokey=f#5
hikey=g#5
pitch_keycenter=g5

<region>
sample=..\libs\SSO\Samples\Oboes\oboes-sus-a#5-PB-loop.wav
lokey=a5
hikey=d6
pitch_keycenter=a#5

// removed C#6, had a click sound (PB)

<region>
sample=..\libs\SSO\Samples\Oboes\oboes-sus-e6-PB-loop.wav
lokey=d#6
hikey=a6 // extend the range (PB)
pitch_keycenter=e6




